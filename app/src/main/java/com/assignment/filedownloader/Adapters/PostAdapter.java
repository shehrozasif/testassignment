package com.assignment.filedownloader.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.assignment.filedownloader.Listeners.ItemClickListener;
import com.assignment.filedownloader.Models.PostItem;
import com.assignment.filedownloader.R;
import com.bumptech.glide.Glide;

import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.MainViewHolder> {

    private List<PostItem> items;
    private ItemClickListener listener;
    private Context context;

    public PostAdapter(Context context, List<PostItem> items, ItemClickListener listener) {
        this.items = items;
        this.listener = listener;
        this.context = context;
    }

    @NonNull
    @Override
    public MainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.post_item_view, parent, false);
        return new MainViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MainViewHolder holder, int position) {
        PostItem item = items.get(position);

        Glide.with(context).load(item.getImageUrl()).into(holder.ivImage);
        holder.tvLikesCounter.setText(item.getImageLikes());

        holder.layout.setOnClickListener(v -> listener.onItemClicked(item));
    }

    public void updateList(List<PostItem> items, int page) {
        if (page == 0) {
            this.items = items;
            notifyDataSetChanged();
        } else {
            this.items.addAll(items);
            notifyDataSetChanged();
        }

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class MainViewHolder extends RecyclerView.ViewHolder {

        ImageView ivImage;
        TextView tvLikesCounter;
        CardView layout;

        MainViewHolder(View view) {
            super(view);
            ivImage = view.findViewById(R.id.ivImage);
            tvLikesCounter = view.findViewById(R.id.tvLikesCounter);
            layout = view.findViewById(R.id.card);

        }
    }
}
