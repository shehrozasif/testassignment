
package com.assignment.filedownloader;

import com.assignment.filedownloader.Models.PostItem;

import java.util.List;

public interface MainView {

    void showProgress();

    void hideProgress();

    void setItems(List<PostItem> items,int page);

    void showMessage(String message);

    void onSwipeRefreshed();
}