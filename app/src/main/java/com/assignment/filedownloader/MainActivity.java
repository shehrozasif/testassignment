package com.assignment.filedownloader;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.assignment.filedownloader.Adapters.PostAdapter;
import com.assignment.filedownloader.Helpers.AppConstants;
import com.assignment.filedownloader.Helpers.Utils;
import com.assignment.filedownloader.Models.PostItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MainView {

    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private MainPresenter presenter;
    private FloatingActionButton fab;
    private SwipeRefreshLayout pullToRefresh;
    private List<PostItem> postItems;
    private PostAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initUi();
    }

    private void initUi() {
        postItems = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclerView);
        progressBar = findViewById(R.id.progressBar);
        pullToRefresh = findViewById(R.id.pullToRefresh);
        fab = findViewById(R.id.fab);
        presenter = new MainPresenter(getApplicationContext(), this);

        adapter = new PostAdapter(this, postItems, presenter::onItemClicked);
        LinearLayoutManager mLayoutManager;
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);

        presenter.setPagination(recyclerView, mLayoutManager);

        loadDataWithPermission();

        setupListeners();
    }

    private void loadDataWithPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.INTERNET},
                    AppConstants.INTERNET_PERMISSION);
        } else
            presenter.loadData(0);
    }

    private void setupListeners() {
        pullToRefresh.setOnRefreshListener(() -> presenter.onSwipeRefresh());

        fab.setOnClickListener(view -> Utils.moveToTop(recyclerView));
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void setItems(List<PostItem> items, int page) {
        adapter.updateList(items, page);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSwipeRefreshed() {
        pullToRefresh.setRefreshing(false);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case AppConstants.INTERNET_PERMISSION: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    presenter.loadData(0);
                } else {
                    Toast.makeText(MainActivity.this, getString(R.string.permission_required), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
