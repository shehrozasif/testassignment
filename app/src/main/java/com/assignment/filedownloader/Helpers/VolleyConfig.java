package com.assignment.filedownloader.Helpers;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class VolleyConfig {

    private static VolleyConfig volleyConf;
    private RequestQueue requestQueue;

    private VolleyConfig(Context context) {
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }

    public static synchronized VolleyConfig getVolleySingleton(Context context) {
        if (volleyConf == null) {
            volleyConf = new VolleyConfig(context);
        }
        return volleyConf;
    }

    public RequestQueue getRequestQueue() {
        return this.requestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

}