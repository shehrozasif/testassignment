package com.assignment.filedownloader.Helpers;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class Utils {

    public static void moveToTop(RecyclerView mRecyclerView) {
        LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        if (layoutManager != null) {
            layoutManager.setSmoothScrollbarEnabled(true);
            layoutManager.smoothScrollToPosition(mRecyclerView, new RecyclerView.State(),0);
        }
    }
}
