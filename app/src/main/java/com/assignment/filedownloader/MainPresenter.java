/*
 *
 *  * Copyright (C) 2018 Antonio Leiva Gordillo.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.assignment.filedownloader;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.assignment.filedownloader.Helpers.AppConstants;
import com.assignment.filedownloader.Helpers.VolleyConfig;
import com.assignment.filedownloader.Models.PostItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

class MainPresenter {

    int firstVisibleItem, visibleItemCount, totalItemCount;
    private MainView mainView;
    private Context context;
    private int page;
    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 5;

    MainPresenter(Context context, MainView mainView) {
        this.mainView = mainView;
        this.context = context;
    }

    void loadData(int page) {
        if (mainView != null) {
            this.page = page;
            mainView.showProgress();
            StringRequest request = new StringRequest(AppConstants.BASE_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String s) {
                    try {
                        parseData(s);
                    } catch (JSONException e) {
                        mainView.onSwipeRefreshed();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    mainView.showMessage(volleyError.getMessage());
                    mainView.onSwipeRefreshed();
                    mainView.hideProgress();
                }
            });
            VolleyConfig.getVolleySingleton(context).addToRequestQueue(request);
        }

    }

    private void parseData(String result) throws JSONException {
        List<PostItem> items = new ArrayList<>();
        JSONArray jsonArray = new JSONArray(result);
        if (page == 0) {
            for (int i = 0; i < visibleThreshold; i++) {
                JSONObject body = jsonArray.getJSONObject(i);
                String likes = body.getString("likes");

                JSONObject innerbody = body.getJSONObject("urls");
                String imageUrl = innerbody.getString("regular");

                items.add(new PostItem(imageUrl, likes));
            }
        } else {
            for (int i = visibleThreshold; i < jsonArray.length(); i++) {
                JSONObject body = jsonArray.getJSONObject(i);
                String likes = body.getString("likes");

                JSONObject innerbody = body.getJSONObject("urls");
                String imageUrl = innerbody.getString("regular");

                items.add(new PostItem(imageUrl, likes));
            }
        }

        mainView.onSwipeRefreshed();
        onFinished(items);
    }

    void onItemClicked(PostItem item) {
        if (mainView != null) {
            mainView.showMessage(item.getImageLikes());
        }
    }

    void onSwipeRefresh() {
        if (mainView != null) {
            previousTotal = 0;
            loadData(0);
        }
    }

    void onDestroy() {
        mainView = null;
    }

    public void onFinished(List<PostItem> items) {
        if (mainView != null) {
            mainView.setItems(items, page);
            mainView.hideProgress();
        }
    }

    public MainView getMainView() {
        return mainView;
    }

    public void setPagination(RecyclerView recyclerView, LinearLayoutManager mLayoutManager) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = 10;
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    // End has been reached
                    // Do something
                    loading = true;
                    loadData(visibleThreshold);
                }
            }
        });
    }
}
