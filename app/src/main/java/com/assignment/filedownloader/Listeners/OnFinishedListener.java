package com.assignment.filedownloader.Listeners;

import com.assignment.filedownloader.Models.PostItem;

import java.util.List;

public interface OnFinishedListener {
    void onFinished(List<PostItem> items);
}
