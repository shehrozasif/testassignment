package com.assignment.filedownloader.Listeners;

import com.assignment.filedownloader.Models.PostItem;

public interface ItemClickListener {
    void onItemClicked(PostItem item);
}
