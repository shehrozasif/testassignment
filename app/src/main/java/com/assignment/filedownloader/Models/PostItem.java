package com.assignment.filedownloader.Models;

public class PostItem {
    private String imageUrl,imageLikes;

    public PostItem(String imageUrl, String imageLikes) {
        this.imageUrl = imageUrl;
        this.imageLikes = imageLikes;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getImageLikes() {
        return imageLikes;
    }
}
